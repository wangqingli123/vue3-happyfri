import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
const ADD_ITEMNUM = 'ADD_ITEMNUM'
const REMBER_ANSWER = 'REMBER_ANSWER'
const REMBER_TIME = 'REMBER_TIME'
const INITIALIZE_DATA = 'INITIALIZE_DATA'
export default new Vuex.Store({
  state: {
	level: '第一周', //活动周数
	itemNum: 1, // 第几题
	itemDetail:[{
		"topic_name":"题目一",
		"topic_answer":[{
			"topic_answer_id": 1,
			"topic_id": 20,
			"answer_name": "答案aaaa",
			"is_standard_answer": 0
		},{
			"topic_answer_id": 2,
			"topic_id": 20,
			"answer_name": "正确答案",
			"is_standard_answer": 0
		},
		{
			"topic_answer_id": 3,
			"topic_id": 20,
			"answer_name": "答案cccc",
			"is_standard_answer": 0
		},
		{
			"topic_answer_id": 4,
			"topic_id": 20,
			"answer_name": "答案dddd",
			"is_standard_answer": 1
		}]
	},{
		"topic_name":"题目二",
		"topic_answer":[{
			"topic_answer_id": 5,
			"topic_id": 21,
			"answer_name": "答案A",
			"is_standard_answer": 1
		},{
			"topic_answer_id": 6,
			"topic_id": 21,
			"answer_name": "答案B",
			"is_standard_answer": 0
		},{
			"topic_answer_id": 7,
			"topic_id": 21,
			"answer_name": "正确答案",
			"is_standard_answer": 0
		},{
			"topic_answer_id": 8,
			"topic_id": 21,
			"answer_name": "答案D",
			"is_standard_answer": 0
		}
		]
	},{
		"topic_name": "题目三",
		"topic_answer":[{
			"topic_answer_id": 9,
			"topic_id": 21,
			"answer_name": "测试A",
			"is_standard_answer": 1
		},{
			"topic_answer_id": 10,
			"topic_id": 21,
			"answer_name": "BBBBBB",
			"is_standard_answer": 0
		},
		{
			"topic_answer_id": 11,
			"topic_id": 21,
			"answer_name": "CCCCCC",
			"is_standard_answer": 0
		},
		{
			"topic_answer_id": 12,
			"topic_id": 21,
			"answer_name": "正确答案",
			"is_standard_answer": 0
		}
		]
	},{
		"topic_name": "题目四",
		"topic_answer":[{
			"topic_answer_id": 13,
			"topic_id": 21,
			"answer_name": "正确答案",
			"is_standard_answer": 1
		},
		{
			"topic_answer_id": 14,
			"topic_id": 21,
			"answer_name": "A是错的",
			"is_standard_answer": 0
		},
		{
			"topic_answer_id": 15,
			"topic_id": 21,
			"answer_name": "D是对的",
			"is_standard_answer": 0
		},{
			"topic_answer_id": 16,
			"topic_id": 21,
			"answer_name": "C说的不对",
			"is_standard_answer": 0
		}
		]
	},{
		"topic_name": "题目五",
		"topic_answer":[{
			"topic_answer_id": 17,
			"topic_id": 21,
			"answer_name": "错误答案",
			"is_standard_answer": 1
		},{
			"topic_answer_id": 18,
			"topic_id": 21,
			"answer_name": "正确答案",
			"is_standard_answer": 0
		},{
			"topic_answer_id": 19,
			"topic_id": 21,
			"answer_name": "错误答案",
			"is_standard_answer": 0
		}, {
			"topic_answer_id": 20,
			"topic_id": 21,
			"answer_name": "错误答案",
			"is_standard_answer": 0
		}
		]
	}
	],
	answer:[],
  },
  mutations: {
	/* increment(state) {
	state.itemNum++;
	} */
	//点击进入下一题
	[ADD_ITEMNUM](state, num) {
		state.itemNum += num;
	},
	//记录答案
	[REMBER_ANSWER](state, id) {
		state.answerid.push(id);
	},
	/*
	记录做题时间
	 */
	[REMBER_TIME](state) {
		state.timer = setInterval(() => {
			state.allTime++;
		}, 1000)
	},
	/*
	初始化信息，
	 */
	[INITIALIZE_DATA](state) {
		state.itemNum = 1;
		state.allTime = 0;
		state.answerid = [];
	}
  },
  actions: {
	addNum({ commit, state }, id) {
		//点击下一题，记录答案id，判断是否是最后一题，如果不是则跳转下一题
		commit('REMBER_ANSWER', id);
		if (state.itemNum < state.itemDetail.length) {
			commit('ADD_ITEMNUM', 1);
		}
	}
  },
  modules: {
  }
})
