import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import item from '../views/item.vue'
/* import account from '../views/account.vue' */

Vue.use(VueRouter)

const routes = [
	{
	path: '/',
	redirect: '/home'
	},
  {
    path: '/home',
    name: 'Home',
    component: Home,
	
  },
  {
	path:'/item',
	name:'item',
	component:item
  }
]

const router = new VueRouter({
  mode:'history',
  routes
})

export default router
